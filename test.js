/* eslint-disable no-unused-expressions */

const { PassThrough, Writable } = require('stream');
const { expect } = require('chai');
const Vinyl = require('vinyl');
const vinylReplace = require('.');

/**
  Sink stream running expectations on any passed vinyl object.
*/
class VinylExpectationStream extends Writable {
  /**
    Creates a new instance of this class.

    @param {Function} expectation - An expectation function
      accepting a vinyl object as its only argument
  */
  constructor(expectation) {
    super({ objectMode: true });

    this.expectation = expectation;
  }

  _write(vin, enc, done) {
    this.expectation(vin);
    done();
  }
}

/**
  Test replace function.
*/
function replaceFunction(input) {
  return input.replace('good', 'bad');
}

/**
  Run a test through the expectation stream.

  @param contents - A Buffer or ReadableStream to use as Vinyl's content
  @param expectation - A function accepting a Vinyl object and running some expectations

  @return The newly created Vinyl objet
*/
function testStream(contents, expectation) {
  const vin = new Vinyl({
    path: '/my/example/path',
    contents
  });
  const replace = vinylReplace(replaceFunction);

  replace.pipe(new VinylExpectationStream(expectation));
  replace.write(vin);

  return vin;
}

describe('VinylReplaceStream', () => {
  const input = 'This is a good expectation';
  const expectedOutput = replaceFunction(input);

  it('should pass null content vinyls through', (done) => {
    testStream(null, (vin) => {
      expect(vin).to.exist;
      done();
    });
  });

  it('should replace contents passed in as a Buffer object', (done) => {
    testStream(Buffer.from(input), (vin) => {
      expect(vin.contents.toString()).to.equal(expectedOutput);
      done();
    });
  });

  it('should replace contents passed in as a Stream object', (done) => {
    const s = new PassThrough({
      readableObjectMode: true,
      writableObjectMode: true,
      allowHalfOpen: false
    });
    const testVin = testStream(s, (vin) => {
      vin.contents.on('readable', () => {
        let data = '';

        for (
          let chunk = vin.contents.read();
          chunk !== null;
          chunk = vin.contents.read()
        ) {
          data += chunk;
        }

        expect(data).to.equal(expectedOutput);
        done();
      });
    });

    testVin.contents.write(input);
  });
});

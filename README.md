# vinyl-replace
A [Vinyl](https://www.npmjs.com/package/vinyl) plugin to replace file contents.

[![pipeline status](https://gitlab.com/qafir/vinyl-replace/badges/master/pipeline.svg)](https://gitlab.com/qafir/vinyl-replace/commits/master)
[![coverage report](https://gitlab.com/qafir/vinyl-replace/badges/master/coverage.svg)](https://gitlab.com/qafir/vinyl-replace/commits/master)

## API

### vinylReplace (transform)
Create a transform stream that applies `transform` to each passed **file** vinyl. Internally, a file vinyl is one with its `contents` property set to either a [Buffer](https://nodejs.org/dist/latest/docs/api/buffer.html) or [Readable](https://nodejs.org/dist/latest/docs/api/stream.html#stream_readable_streams) stream object. Any other vinyl is passed through as is.

#### The transform function
The transform function is any callable object accepting as input a string (the vinyl's content) and returning a possibly new version of it, that will be replaced as the new content of the vinyl.


## Example
```javascript
const { src, dest } = require('gulp');
const vinylReplace = require('vinyl-replace');

function myTask () {
  return src('*.txt')
    .pipe(vinylReplace((input) => {
      return input.replace('something', 'everything');
    }))
    .pipe(dest('dist/'));
}

```

## License
This project is distributed under the BSD 3 Clause license (BSD-3-Clause). Please see the [LICENSE](LICENSE) file.

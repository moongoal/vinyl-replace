/* eslint-disable class-methods-use-this */

const { Transform, Readable } = require('stream');

class VinylReplaceStream extends Transform {
  constructor(opts) {
    super({ objectMode: true });

    this.opts = opts;
  }

  _transformFromStream(vin, done) {
    const s = vin.contents;

    s.on('readable', () => {
      let data = '';
      let dataAvailable = true;

      for (let chunk = s.read(); chunk !== null; chunk = s.read()) {
        data += chunk;
      }

      const interpolatedStr = this._transformString(data);
      const newVin = vin.clone({ contents: false });
      newVin.contents = new Readable({
        objectMode: true,
        read() {
          if (dataAvailable) {
            this.push(interpolatedStr);
          }

          dataAvailable = false;
        }
      });

      done(
        null,
        newVin
      );
    });
  }

  _transformFromBuffer(vin, done) {
    const str = vin.contents.toString();
    const interpolatedStr = this._transformString(str);
    const newVin = vin.clone({ contents: false });

    newVin.contents = Buffer.from(interpolatedStr);

    done(null, newVin);
  }

  _transformFromNull(vin, done) {
    // Not our business, just pass through
    done(null, vin);
  }

  _transformString(s) {
    return this.opts.transform(s);
  }

  _transform(vin, enc, done) {
    let transform;

    if (vin.isBuffer()) {
      transform = this._transformFromBuffer;
    } else if (vin.isStream()) {
      transform = this._transformFromStream;
    } else {
      transform = this._transformFromNull;
    }

    transform.call(this, vin, done);
  }
}

module.exports = function vinylReplace(transform) {
  return new VinylReplaceStream({ transform });
};
